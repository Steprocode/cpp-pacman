#pragma once

#include "Ghost.h"
#include "Board.h"
#include "Definitions.h"

namespace PacMan {
class Pinky : public Ghost {
public:
    Pinky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Pinky();
    void update(float dt) override;
};
}
