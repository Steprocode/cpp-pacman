#pragma once

#include <QPainter>
#include <QString>
#include <QPointF>
#include <vector>
#include <cmath>
#include "Definitions.h"
#include "Board.h"
#include "Entity.h"
#include "AssetsLoader.h"
#include "Cell.h"
#include "NewPathfinder.h"
#include "Player.h"
#include "ClippedPoint.h"

namespace PacMan {
    
class Player;
class Ghost : public Entity {
public:
    enum Mode {NO_MODE, FRIGHTENED, CHASE, SCATTER};
    enum ChaseTarget {NO_TARGET, PLAYER, PLAYER_AHEAD, PLAYER_BACK};
    Ghost(unsigned int row, unsigned int col, Board &board, QPointF navigatorPos, QString tilesPath, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Ghost() override;
    virtual void update(float delta) override;
    void setPlayer(Player *player);
    Mode getMode() const;
    void setMode(Mode mode);
protected:
    void handleCollisionWithPlayer() const;
    Direction determineDirectionToCell(unsigned int targetRow, unsigned int targetCol) const;
    Direction determineDirectionToCell(const Cell *targetCell) const;
    void refreshPixmap(float delta) override;
    Dictionary dictionary;
    QPixmap frightenedPixmap;
    Player *player = NULL;
    Mode mode = NO_MODE;
    ChaseTarget target = NO_TARGET;
    Cell *targetCell = NULL;
private:
    void handleChaseMode();
    void handleFrightenedMode();
    void handleScatterMode();
};
}
