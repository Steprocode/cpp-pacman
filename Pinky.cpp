#include "Pinky.h"

namespace PacMan {
Pinky::Pinky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment) 
: Ghost(row, col, board, QPoint(NAVIGATOR_POS_X, NAVIGATOR_POS_Y), PINKY_TILES_PATH, initPixKey, spawnAlignment) {
    speed = 40;
    mode = CHASE;
    target = PLAYER_AHEAD;
}

Pinky::~Pinky() {
    
}

void Pinky::update(float dt) {
    Ghost::update(dt);
}
}
