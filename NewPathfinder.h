#pragma once

#include <QString>
#include <QDebug>
#include <vector>
#include <cstdlib>
#include <limits>
#include <map>
#include <cmath>
#include <functional>
#include <queue>
#include "Cell.h"
#include "Board.h"

namespace PacMan {
class NewPathfinder {
    class Node;
public:
    NewPathfinder(QString IDMask, Board &board);
    ~NewPathfinder();
    std::vector<const Cell *> findRoute(unsigned int initRow, unsigned int initCol, unsigned int targetRow, unsigned int targetCol) const;
    std::vector<const Cell *> findRoute(const Cell *initCell, const Cell *targetCell) const;
private:
    Board &board;
    QString IDMask;
};

class NewPathfinder::Node {
public:
    Node(const Cell *cell, const Cell *targetCell, QString IDMask, const Node *parent = nullptr);
    Node *dig();
    std::vector<const Cell *> convertToArray() const;
    ~Node();
private:
    bool isAncestor(const Cell *cell) const;
    bool isChild(const Cell *cell) const;
    
    const Cell *cell, *targetCell;
    QString IDMask;
    const Node *parent;
    vector<Node> children;
    bool isExplored = false;
};
}
