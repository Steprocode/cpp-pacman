#pragma once
#include <string>
#include <cmath>
#include <ctime>
#include <QApplication>
#include <QPushButton>
#include <QImage>
#include <QPainter>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QPixmap>
#include <QLabel>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFontDatabase>
#include <QFont>
#include <QSpacerItem>
#include <QSizePolicy>
#include "Tiles.h"
#include "GameArea.h"
#include "Player.h"
#include "Definitions.h"
#include "GameManager.h"

namespace PacMan {
class Game : public QWidget {
    Q_OBJECT
public:
    Game(QWidget *parent = 0);
    ~Game();
    void start();
    virtual int heightForWidth(int width) const override;
    virtual bool hasHeightForWidth() const override;
    virtual QSize sizeHint() const override;
private slots:
    void refreshScoreLabel(int score);
    void displayFinishLabel();
private:
    QVBoxLayout *contents;
    GameArea *gameArea;
    QLabel *scoreLabel = NULL;
    QVBoxLayout *scoreLayout;
    QHBoxLayout *topLayout;
    QLabel *scoreTitleLabel, *finishLabel;
};
}
