#include "Board.h"

namespace PacMan {
    
Board::Board(QString tilesDictionaryPath, QString tilesPath) 
: GameObject(0, 0, BASE_CELL_WIDTH, BASE_CELL_HEIGHT), dictionary(AssetsLoader::loadDictionary(tilesDictionaryPath, tilesPath)) {
    
}

Board::~Board() {
    clearCells();
}

void Board::refresh() {
    setWidth(cols * cellWidth);
    setHeight(rows * cellHeight);
}

void Board::update(float /*delta*/) {
    
}

void Board::draw(float /*delta*/, float scale, QPainter &painter) const {
    if (cells != NULL)
        for (unsigned int row = 0; row < rows; row++)
            for (unsigned int col = 0; col < cols; col++)
                cells[row][col]->draw(0, scale, painter);
}

void Board::loadMap(const QString &mapPath) {
    clearCells();
    QString input = AssetsLoader::readFile(mapPath);
    QSize size = AssetsLoader::determineSize(input);
    cols = size.width();
    rows = size.height();
    
    cells = new Cell**[rows];
    for (unsigned int row = 0; row < rows; row++) {
        cells[row] = new Cell*[cols];
    }
    for (unsigned int row = 0; row < rows; row++) {
        for (unsigned int col = 0; col < cols; col++)
            cells[row][col] = new Cell(row, col, cellWidth, cellHeight, dictionary);
    }
    
    QChar **matrix = AssetsLoader::passInputToMatrix(rows, cols, input);
    for (unsigned int row = 0; row < rows; row++)
        for (unsigned int col = 0; col < cols; col++) 
            if (matrix[row][col] != ' ')
                cells[row][col]->setID(matrix[row][col]);
    
    for (unsigned int row = 0; row < rows; row++)
        delete[](matrix[row]);
    delete[](matrix);
    
    connectCells();
    refresh();
}

void Board::connectCells() {
    if (cells == NULL || rows == 0 || cols == 0)
        return;
    
    for (unsigned int row = 0; row < rows; row++) {
        for (unsigned int col = 0; col < cols; col++) {
            int left = col - 1;
            if (left < 0)
                left += cols;
            
            int right = col + 1;
            right %= cols;
            
            int up = row - 1;
            if (up < 0)
                up += rows;
            
            int bottom = row + 1;
            bottom %= rows;
            
            cells[row][col]->rightCell = cells[row][right];
            cells[row][col]->leftCell = cells[row][left];
            cells[row][col]->topCell = cells[up][col];
            cells[row][col]->bottomCell = cells[bottom][col];
        }
    }
}

void Board::clearCells() {
    if (cells != NULL) {
        for (unsigned int row = 0; row < rows; row++) {
            for (unsigned int col = 0; col < cols; col++)
                delete(cells[row][col]);
            delete[](cells[row]);
        }
        delete[](cells);
    }
    rows = 0;
    cols = 0;
}

Cell *Board::getCellContaining(const QPointF &point) {
    ClippedPoint clippedPoint(point, *this);
    
    for (unsigned int row = 0; row < rows; row++)
        for (unsigned int col = 0; col < cols; col++)
            if (cells[row][col]->contains(clippedPoint.toPoint()))
                return cells[row][col];
    return NULL;
}


unsigned int Board::getCellWidth() const {
    return cellWidth;
}

unsigned int Board::getCellHeight() const {
    return cellHeight;
}

unsigned int Board::getRows() const {
    return rows;
}

unsigned int Board::getCols() const {
    return cols;
}

Cell ***Board::getCells() const {
    return cells;
}


}
