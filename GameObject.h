#pragma once

#include <QRectF>
#include <QRect>
#include <QPainter>
#include <QSize>
#include <QPointF>
#include <QGraphicsItem>
#include <math.h>

namespace PacMan {
class GameObject : public QRectF {
public:
    GameObject();
    GameObject(float x, float y, float height, float width);
    GameObject(const QRectF &rect);
    virtual ~GameObject();
    QRect getScaledRect(float scale) const;
    QRectF getScaledRectF(float scale) const;
protected:
    virtual void update(float delta) = 0;
    virtual void draw(float delta, float scale, QPainter &painter) const = 0;
    QRect determineScaledRect(float scale) const;
    QRect determineScaledRect(float scale, const QRectF &rect) const;
    QRectF determineScaledRectF(float scale) const;
    QRectF determineScaledRectF(float scale, const QRectF &rect) const;
private:
};

}
