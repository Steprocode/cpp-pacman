#include "GameManager.h"

namespace PacMan {
    
GameManager::GameManager(QObject* parent) 
: QObject(parent) {
}

GameManager::GameManager(Board &board, QObject* parent)
: QObject(parent), board(&board) {
}

GameManager::~GameManager() {
    if (player != NULL)
        delete(player);
    for (Ghost *ghost: ghosts)
        delete(ghost);
    
}

void GameManager::updateBallsLeft() {
    ballsLeft = 0;
    for (unsigned int row = 0; row < board->getRows(); row++)
        for (unsigned int col = 0; col < board->getCols(); col++)
            checkCellForBalls(row, col, *board);
}

void GameManager::checkCellForBalls(unsigned int row, unsigned int col, Board &board) {
    QChar id = board.getCells()[row][col]->getID();
    if (id == '*') {
        ballsLeft++;
    }
    else if (id == '@') {
        ballsLeft++;
    }
}

void GameManager::spawnEntities() {
    for (unsigned int row = 0; row < board->getRows(); row++)
        for (unsigned int col = 0; col < board->getCols(); col++)
            checkCellForEntities(row, col, *board);
}

void GameManager::checkCellForEntities(unsigned int row, unsigned int col, Board &board) {
    QChar id = board.getCells()[row][col]->getID();
    if (id == 'V') {
        player = new Player(row, col, board, L'◀', Entity::RIGHT_SIDED);
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'v') {
        player = new Player(row, col, board, L'◀', Entity::LEFT_SIDED);
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'B') {
        ghosts.push_back(new Blinky(row, col, board, L'◀', Entity::RIGHT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'b') {
        ghosts.push_back(new Blinky(row, col, board, L'◀', Entity::LEFT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'P') {
        ghosts.push_back(new Pinky(row, col, board, L'▼', Entity::RIGHT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'p') {
        ghosts.push_back(new Pinky(row, col, board, L'▼', Entity::LEFT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'C') {
        ghosts.push_back(new Clyde(row, col, board, L'▲', Entity::RIGHT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'c') {
        ghosts.push_back(new Clyde(row, col, board, L'▲', Entity::LEFT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'I') {
        ghosts.push_back(new Inky(row, col, board, L'▲', Entity::RIGHT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    else if (id == 'i') {
        ghosts.push_back(new Inky(row, col, board, L'▲', Entity::LEFT_SIDED));
        board.getCells()[row][col]->setID(L'□');
    }
    for (Ghost *ghost: ghosts)
        ghost->setMode(globalMode);
    setPlayerActions();
}

void GameManager::update(float delta) {
    updateMode(delta);
    if (player != NULL)
            player->update(delta);
    for (Ghost *ghost: ghosts)
        ghost->update(delta);
}

void GameManager::draw(float delta, float scale, QPainter &painter) {
    if (player != NULL)
        player->draw(delta, scale, painter);
    for (Ghost *ghost: ghosts)
        ghost->draw(delta, scale, painter);
}

void GameManager::updateMode(float delta) {
    timeToModeChange -= delta;
    if (timeToModeChange < 0) {
        if (globalMode == Ghost::FRIGHTENED) {
            globalMode = Ghost::CHASE;
            timeToModeChange = 20;
        }
        else if (globalMode == Ghost::SCATTER) {
            globalMode = Ghost::CHASE;
            timeToModeChange = 20;
        }
        else {
            globalMode = Ghost::SCATTER;
            timeToModeChange = 15;
        }
        for (Ghost *ghost: ghosts)
            ghost->setMode(globalMode);
    }
}

void GameManager::handleKeyEvent(QKeyEvent *event) {
    player->handleKeyEvent(event);
}

void GameManager::addScore(int increment) {
    score += increment;
    emit scoreValueChanged(score);
}

void GameManager::setScore(int score) {
    this->score = score;
}

int GameManager::getScore() const {
    return score;
}

int GameManager::getBallsLeft() const {
    return ballsLeft;
}

bool GameManager::areAnyBallsLeft() const {
    return ballsLeft > 0;
}

void GameManager::reset() {
    for (Ghost *ghost: ghosts)
        ghost->reset();
    if (player != NULL) {
        player->reset();
    }
}

void GameManager::setPlayerActions() {
    if (player != NULL) {
        for (Ghost *ghost: ghosts)
            ghost->setPlayer(player);
        
        player->setActionOnEatenObject([this](QChar symb) {
            int reward = 0;
            if (symb == '*') {
                reward = 10;
                addScore(reward);
                ballsLeft--;
            }
            else if (symb == '@') {
                reward = 50;
                addScore(reward);
                globalMode = Ghost::FRIGHTENED;
                for (Ghost *ghost: ghosts)
                    ghost->setMode(globalMode);
                timeToModeChange = 15;
                ballsLeft--;
            }
            
        });
        player->setActionOnBeingEaten([this](const Ghost *ghost) {
            handlePlayerCollision(ghost);
        });
    }
}

void GameManager::handlePlayerCollision(const Ghost *collidingGhost) {
    for (Ghost *ghost: ghosts)
        if (ghost == collidingGhost) {
            if (ghost->getMode() == Ghost::FRIGHTENED) {
                ghost->reset();
                ghost->setMode(Ghost::SCATTER);
                int ghostEatenScore = 200;
                addScore(ghostEatenScore);
            }
            else {
                int scorePenalty = -500;
                addScore(scorePenalty);
                reset();
            }
        }
}

}
