#include "Tiles.h"

namespace PacMan {

Tiles::Tiles(unsigned int rows, unsigned int cols, QString path)
: path(path), rows(rows), cols(cols) {
    
    QImageReader imgReader(path);
    tileWidth = imgReader.size().width() / cols;
    tileHeight = imgReader.size().height() / rows;
}

Tiles::Tiles(QString path, unsigned int tileWidth, unsigned int tileHeight)
: path(path), tileWidth(tileWidth), tileHeight(tileHeight) {
    QImageReader imgReader(path);
    cols = imgReader.size().width() / tileWidth;
    rows = imgReader.size().height() / tileHeight;
}

Tiles::~Tiles() { }

QImage Tiles::load(unsigned int row, unsigned int col) const {
    QImageReader imgReader(path);
    imgReader.setClipRect(QRect(col * tileWidth, row * tileHeight, tileWidth, tileHeight));
    QImage img = imgReader.read();
    return img;
}

unsigned int Tiles::getTileWidth() const {
    return tileWidth;
}

unsigned int Tiles::getTileHeight() const {
    return tileHeight;
}

}
