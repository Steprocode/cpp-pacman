#include "Inky.h"

namespace PacMan {
Inky::Inky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment) 
: Ghost(row, col, board, QPoint(NAVIGATOR_POS_X, NAVIGATOR_POS_Y), INKY_TILES_PATH, initPixKey, spawnAlignment) {
    speed = 40;
    mode = CHASE;
    target = PLAYER; //random
}

Inky::~Inky() {
    
}

void Inky::update(float dt) {
    Ghost::update(dt);
    if (timeToChangeTactics < 0) {
        int randTarget = rand() % 3;
        if (randTarget == 0)
            target = PLAYER;
        else if (randTarget == 1)
            target = PLAYER_BACK;
        else
            target = PLAYER_AHEAD;
        
        int randChangeTime = (rand() % 10) - 5;
        int changeTime = 20 + randChangeTime;
        timeToChangeTactics = changeTime;
    }
    else
        timeToChangeTactics -= dt;
}
}
