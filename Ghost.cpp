#include "Ghost.h"

namespace PacMan {
Ghost::Ghost(unsigned int row, unsigned int col, Board &board, QPointF navigatorPos, QString tilesPath, QChar initPixKey, SpawnAlignment spawnAlignment)
: Entity(row, col, board, navigatorPos, initPixKey, spawnAlignment), dictionary(AssetsLoader::loadDictionary(GHOST_DICT_PATH, tilesPath)) {
    setPixmap(dictionary.at(initPixKey));
    allowedCellIDs += "▁▔▏▕";
    frightenedPixmap = QPixmap(FRIGHTENED_GHOST_PATH);
}

Ghost::~Ghost() {
    
}

void Ghost::update(float delta) {
    Entity::update(delta);
    handleCollisionWithPlayer();
    
    if (mode == CHASE)
        handleChaseMode();
    else if (mode == FRIGHTENED)
        handleFrightenedMode();
    else if (mode == SCATTER)
        handleScatterMode();
    if (targetCell != NULL && allowedCellIDs.contains(targetCell->getID()))
        nextDirection = determineDirectionToCell(targetCell);
}

void Ghost::handleFrightenedMode() {
    if (player == NULL) {
        handleScatterMode();
        return;
    }
    QPointF playerPos = player->center();
    ClippedPoint target(board);
    target.setX(playerPos.x() + board.width() / 2);
    target.setY(playerPos.y() + board.height() / 2);
    Cell *targetCell = board.getCellContaining(target.toPointF());
    vector<Cell *> neighborCells = targetCell->getNeighbors(allowedCellIDs);
    if (allowedCellIDs.contains(targetCell->getID()))
        this->targetCell = targetCell;
    else if (neighborCells.size() > 0)
        this->targetCell = neighborCells[0];
    else
        handleScatterMode();
}

void Ghost::handleScatterMode() {
    Cell *currentCell = board.getCellContaining(center());
    vector<Cell *> neighborCells = currentCell->getNeighbors(allowedCellIDs);
    
    if (currentDirection == HOLD || nextDirection == HOLD || currentCell == targetCell) {
        QPointF randPos(rand() % (int) round(board.width()), rand() % (int) round(board.height()));
        ClippedPoint target(board);
        target.setX(randPos.x() + board.width() / 2);
        target.setY(randPos.y() + board.height() / 2);
        Cell *targetCell = board.getCellContaining(target.toPointF());
        vector<Cell *> neighborTargetCells = targetCell->getNeighbors(allowedCellIDs);
        if (allowedCellIDs.contains(targetCell->getID()))
            this->targetCell = targetCell;
        else if (neighborTargetCells.size() > 0)
            this->targetCell = neighborTargetCells[0];
        else
            handleScatterMode();
    }
}

void Ghost::handleChaseMode() {
    if (player != NULL && mode != NO_MODE && target != NO_TARGET) {
        Cell* playerCell = board.getCellContaining(player->center());
        Cell* targetCell = playerCell;
        if (target == PLAYER_AHEAD) {
            Cell *oldTarget = NULL;
            vector<Cell *> neighbors = targetCell->getNeighbors(allowedCellIDs);
            while (neighbors.size() == 2 && targetCell != oldTarget) {
                oldTarget = targetCell;
//                 if (targetCell->contains(center())) {
//                     targetCell = playerCell;
//                     break;
//                 }
                for (Cell* neighbor: neighbors) {
                    if ((player->getDirection() == UP && targetCell->topCell == neighbor)
                        || (player->getDirection() == DOWN && targetCell->bottomCell == neighbor)
                        || (player->getDirection() == LEFT && targetCell->leftCell == neighbor)
                        || (player->getDirection() == RIGHT && targetCell->rightCell == neighbor)) {
                        targetCell = neighbor;
                        neighbors = targetCell->getNeighbors(allowedCellIDs);
                    }
                }
            }
        }
        else if (target == PLAYER_BACK) {
            Cell *oldTarget = NULL;
            vector<Cell *> neighbors = targetCell->getNeighbors(allowedCellIDs);
            while (neighbors.size() == 2 && targetCell != oldTarget) {
                oldTarget = targetCell;
//                 if (targetCell->contains(center())) {
//                     targetCell = playerCell;
//                     break;
//                 }
                for (Cell* neighbor: neighbors) {
                    if ((player->getDirection() == UP && targetCell->bottomCell == neighbor)
                        || (player->getDirection() == DOWN && targetCell->topCell == neighbor)
                        || (player->getDirection() == LEFT && targetCell->rightCell == neighbor)
                        || (player->getDirection() == RIGHT && targetCell->leftCell == neighbor)) {
                        targetCell = neighbor;
                        neighbors = targetCell->getNeighbors(allowedCellIDs);
                    }
                }
            }
        }
        this->targetCell = targetCell;
    }
}

void Ghost::refreshPixmap(float /*delta*/) {
    if (mode == FRIGHTENED) {
        if (pixmap != frightenedPixmap)
            setPixmap(frightenedPixmap);
    }
    else if (currentDirection == UP) {
        if (pixmap != dictionary.at(L'▲'))
            setPixmap(dictionary.at(L'▲'));
    }
    else if (currentDirection == DOWN) {
        if (pixmap != dictionary.at(L'▼'))
            setPixmap(dictionary.at(L'▼'));
    }
    else if (currentDirection == LEFT) {
        if (pixmap != dictionary.at(L'◀'))
            setPixmap(dictionary.at(L'◀'));
    }
    else if (currentDirection == RIGHT) {
        if (pixmap != dictionary.at(L'▶'))
            setPixmap(dictionary.at(L'▶'));
    }
}

Entity::Direction Ghost::determineDirectionToCell(unsigned int targetRow, unsigned int targetCol) const {
    const Cell *targetCell = board.getCells()[targetRow][targetCol];
    return determineDirectionToCell(targetCell);
}

Entity::Direction Ghost::determineDirectionToCell(const Cell *targetCell) const {
    const Cell *initCell = board.getCellContaining(center());
    NewPathfinder pathfinder(allowedCellIDs, board);
    std::vector<const Cell *> path = pathfinder.findRoute(initCell, targetCell);
    if (path.size() > 1) {
        const Cell *current = path.at(0);
        const Cell *next = path.at(1);
        if (current->topCell == next) {
            return UP;
        }
        else if (current->bottomCell == next) {
            return DOWN;
        }
        else if (current->leftCell == next) {
            return LEFT;
        }
        else if (current->rightCell == next) {
            return RIGHT;
        }
    }
    return nextDirection;
}

void Ghost::setPlayer(Player *player) {
    this->player = player;
}

void Ghost::handleCollisionWithPlayer() const {
    if (player == NULL)
        return;
    
    if (collides(*player)) {
        player->eaten(this);
    }
}

Ghost::Mode Ghost::getMode() const {
    return mode;
}

void Ghost::setMode(Ghost::Mode mode) {
    this->mode = mode;
}

}
