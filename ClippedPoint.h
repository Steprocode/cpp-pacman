#pragma once

#include <QPointF>
#include <QRectF>
#include <QPoint>

namespace PacMan {
class ClippedPoint : private QPointF {
public:
    ClippedPoint();
    ClippedPoint(QRectF clip);
    ClippedPoint(QPointF qPoint, QRectF clip);
    ClippedPoint(float x, float y, QRectF clip);
    ~ClippedPoint();
    void refresh();
    QRectF getClip() const;
    void setClip(QRectF clip);
    float x() const;
    float y() const;
    void setX(float x);
    void setY(float y);
    QPointF toPointF() const;
    QPoint toPoint() const;
private:
    QRectF clip;
};
}
