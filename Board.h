#pragma once

#include <QGridLayout>
#include <QSize>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QChar>
#include <QImageReader>
#include <QMap>
#include <QWidget>
#include <QSizePolicy>
#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QPointF>
#include "Tiles.h"
#include "Cell.h"
#include "Definitions.h"
#include "AssetsLoader.h"
#include "ClippedPoint.h"

namespace PacMan {
class Cell;

class Board : public GameObject {
public:
    Board(QString tilesDictionaryPath, QString tilesPath);
    ~Board() override;
    void loadMap(const QString &mapPath);
    void refresh();
    
    void update(float delta) override;
    void draw(float delta, float scale, QPainter &painter) const override;
    
    unsigned int getCellWidth() const;
    unsigned int getCellHeight() const;
    unsigned int getRows() const;
    unsigned int getCols() const;
    Cell ***getCells() const;
    Cell *getCellContaining(const QPointF &point);
protected:
private:
    Cell ***cells = NULL;
    Dictionary dictionary;
    unsigned int rows, cols, cellWidth = BASE_CELL_WIDTH, cellHeight = BASE_CELL_HEIGHT;
    void clearCells();
    void connectCells();
};
}
