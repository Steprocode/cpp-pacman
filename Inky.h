#pragma once

#include <cmath>
#include "Ghost.h"
#include "Board.h"
#include "Definitions.h"

namespace PacMan {
class Inky : public Ghost {
public:
    Inky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Inky();
    void update(float dt) override;
private:
    float timeToChangeTactics = 15;
};
}
