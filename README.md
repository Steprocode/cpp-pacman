# Cpp-PacMan
A simple PacMan-like game written in C++ using Qt5.

## Table of contents
* [Dependencies](#dependencies)
* [Setup](#setup)

## Dependencies
Program requires following libraries to be installed:

* qt5-qtbase-devel (alternatively qt5-devel)
	
## Setup
Compile the project:
```
$ qmake-qt5
$ make
```

Run the compiled program:
```
$ ./pacman
```

To remove files after compilation:
```
$ make clean
```
