#include "Blinky.h"

namespace PacMan {
Blinky::Blinky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment) 
: Ghost(row, col, board, QPoint(NAVIGATOR_POS_X, NAVIGATOR_POS_Y), BLINKY_TILES_PATH, initPixKey, spawnAlignment) {
    speed = 40;
    mode = CHASE;
    target = PLAYER;
}

Blinky::~Blinky() {
    
}

void Blinky::update(float delta) {
    Ghost::update(delta);
    
}
}
