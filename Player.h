#pragma once

#include <algorithm>
#include <QPen>
#include <QPointF>
#include <QKeyEvent>
#include <QPixmap>
#include <QChar>
#include "Entity.h"
#include "AssetsLoader.h"
#include "Definitions.h"
#include "Ghost.h"

#define PLAYER_ANIMATION_TIME 0.25

namespace PacMan {

class Ghost;
class Player : public Entity {
public:
    Player(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Player() override;
    void update(float delta) override;
    void draw(float delta, float scale, QPainter &painter) const override;
    void handleKeyEvent(QKeyEvent *event) override;
    void setActionOnEatenObject(function<void(QChar)> anonymous);
    void setActionOnBeingEaten(function<void(const Ghost*)> anonymous);
    void eaten(const Ghost *ghost);
    virtual void reset() override; 
protected:
    void refreshPixmap(float delta) override;
private:
    float animationTimer = PLAYER_ANIMATION_TIME;
    bool isActionOnEatenObjectDefined = false;
    bool isActionOnBeingEatenDefined = false;
    Dictionary dictionary;
    function<void(QChar)> actionOnEatenObject;
    function<void(const Ghost*)> actionOnBeingEaten;
};

}
