#include "Player.h"

namespace PacMan {

Player::Player(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment) 
: Entity(row, col, board, QPoint(NAVIGATOR_POS_X, NAVIGATOR_POS_Y), initPixKey, spawnAlignment), dictionary(AssetsLoader::loadDictionary(PACMAN_DICT_PATH, PACMAN_TILES_PATH)) {
    setPixmap(dictionary.at(QChar(L'◀')));
}

Player::~Player() {
    
}

void Player::update(float delta) {
    Entity::update(delta);
    
    Cell *cell = board.getCellContaining(getNaviBorderPoint());
    if (cell != NULL) {
        if (cell->getID() == '*') {
            if (isActionOnEatenObjectDefined)
                actionOnEatenObject('*');
            cell->setID(QChar(L'□'));
        }
        else if(cell->getID() == '@') {
            if (isActionOnEatenObjectDefined)
                actionOnEatenObject('@');
            cell->setID(QChar(L'□'));
        }
    }
}

void Player::draw(float delta, float scale, QPainter &painter) const {
    Entity::draw(delta, scale, painter);
}

void Player::handleKeyEvent(QKeyEvent *event) {
    int key = event->key();
    if (key == Qt::Key_Up) {
        nextDirection = UP;
    }
    else if (key == Qt::Key_Down) {
        nextDirection = DOWN;
    }
    else if (key == Qt::Key_Left) {
        nextDirection = LEFT;
    }
    else if (key == Qt::Key_Right) {
        nextDirection = RIGHT;
    }
}


void Player::refreshPixmap(float delta) {
    if (currentDirection == UP) {
        if (pixmap != dictionary.at(QChar(L'▲')))
            setPixmap(dictionary.at(QChar(L'▲')));
    }
    else if (currentDirection == DOWN) {
        if (pixmap != dictionary.at(QChar(L'▼')))
            setPixmap(dictionary.at(QChar(L'▼')));
    }
    else if (currentDirection == LEFT) {
        if (pixmap != dictionary.at(QChar(L'◀')))
            setPixmap(dictionary.at(QChar(L'◀')));
    }
    else if (currentDirection == RIGHT) {
        if (pixmap != dictionary.at(QChar(L'▶')))
            setPixmap(dictionary.at(QChar(L'▶')));
    }
    animationTimer -= delta;
    if (animationTimer < 0 && animationTimer > -PLAYER_ANIMATION_TIME)
        setPixmap(dictionary.at(QChar(L'■')));
    else if (animationTimer <= -PLAYER_ANIMATION_TIME)
        animationTimer = PLAYER_ANIMATION_TIME;
}

void Player::setActionOnEatenObject(function<void(QChar)> anonymous) {
    this->actionOnEatenObject = anonymous;
    isActionOnEatenObjectDefined = true;
}

void Player::setActionOnBeingEaten(function<void(const Ghost*)> anonymous) {
    this->actionOnBeingEaten = anonymous;
    isActionOnBeingEatenDefined = true;
}

void Player::eaten(const Ghost *ghost) {
    if (isActionOnBeingEatenDefined)
        actionOnBeingEaten(ghost);
}

void Player::reset() {
    Entity::reset();
    setPixmap(dictionary.at(QChar(L'◀')));
}

}
