#include "AssetsLoader.h"

namespace PacMan {

Dictionary AssetsLoader::loadDictionary(const QString &tilesDictionaryPath, const QString &tilesPath) {
    Dictionary dictionary;
    QString input = readFile(tilesDictionaryPath);
    QSize size = determineSize(input);
    int rows = size.height(), cols = size.width();
    
    QChar **matrix = passInputToMatrix(rows, cols, input);
    
    Tiles tileset(rows, cols, tilesPath);
    for (int row = 0; row < rows; row++)
        for (int col = 0; col < cols; col++) 
            if (matrix[row][col] != ' ')
                dictionary[QChar(matrix[row][col])] = QPixmap::fromImage(tileset.load(row, col));
        
    for (int row = 0; row < rows; row++)
        delete[](matrix[row]);
    delete[](matrix);
    return dictionary;
}

QString AssetsLoader::readFile(const QString &path) {
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) 
        qCritical() << file.errorString();
    QTextStream stream(&file);
    QString input = stream.readAll();
    file.close();
    return input;
}

QChar **AssetsLoader::passInputToMatrix(unsigned int rows, unsigned int cols, const QString &text) {
    QChar **matrix = new QChar*[rows];
    for (unsigned int row = 0; row < rows; row++) {
        matrix[row] = new QChar[cols];
    }
    for (unsigned int row = 0; row < rows; row++)
        for (unsigned int col = 0; col < cols; col++)
            matrix[row][col] = ' ';
        
    if (text.size() == 0)
        return matrix;
    
    int counter = 0;
    unsigned int row = 0;
    unsigned int col = 0;
    for (QChar current: text) {
        if (current == '\0' || row >= rows) {
            break;
        }
        else if (current == '\n' || col >= cols) {
            row += 1;
            col = 0;
            continue;
        }
        
        matrix[row][col] = current;
        
        col++;
        counter++;
    }
    return matrix;
}

QSize AssetsLoader::determineSize(const QString &text) {
    unsigned int width = 0, height = 0;
    if (text.size() == 0)
        return QSize(width, height);
    int counter = -1;
    QChar current;
    do {
        counter++;
        current = text[counter];
        unsigned int currentCols = 0;
        if (counter < text.size() && current != '\0' && current != '\n')
            height++;
        
        while (counter < text.size() && current != '\0' && current != '\n') {
            currentCols++;
            counter++;
            current = text[counter];
        }
        if (currentCols > width)
            width = currentCols;
        
    } while (counter < text.size() && current != '\0');
    
    return QSize(width, height);
}

}
