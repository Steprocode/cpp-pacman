#include "Cell.h"

namespace PacMan {
    
Cell::Cell(const Dictionary &dictionary)
: GameObject(), dictionary(dictionary) { }

Cell::Cell(QChar ID, const Dictionary &dictionary)
: GameObject(), ID(ID), dictionary(dictionary) { }
    
Cell::Cell(unsigned int row, unsigned int col, float width, float height, const Dictionary &dictionary)
: GameObject(col * width, row * height, width, height), dictionary(dictionary), row(row), col(col) { }

Cell::~Cell() {
    
}

QChar Cell::getID() const {
    return ID;
}

void Cell::setID(QChar ID) {
    this->ID = ID;
    if (dictionary.find(ID) != dictionary.end())
        pixmap = dictionary.at(ID);
}

void Cell::update(float /*delta*/) {
    
}

void Cell::draw(float /*delta*/, float scale, QPainter& painter) const {
    painter.drawPixmap(determineScaledRect(scale), pixmap);
}

vector<Cell *> Cell::getNeighbors(QString IDMask) const {
    vector<Cell *> neighbours;
    if (topCell != NULL && IDMask.contains(topCell->getID()))
        neighbours.push_back(topCell);
    if (bottomCell != NULL && IDMask.contains(bottomCell->getID()))
        neighbours.push_back(bottomCell);
    if (leftCell != NULL && IDMask.contains(leftCell->getID()))
        neighbours.push_back(leftCell);
    if (rightCell != NULL && IDMask.contains(rightCell->getID()))
        neighbours.push_back(rightCell);
    return neighbours;
}

unsigned int Cell::getRow() const {
    return row;
}

unsigned int Cell::getCol() const {
    return col;
}

ostream &operator<<(ostream &output, const Cell &cell) { 
    output << "row: " << cell.getRow() << ", col: " << cell.getCol();
    return output;            
}
    
}
