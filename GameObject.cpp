#include "GameObject.h"

namespace PacMan {

GameObject::GameObject()
: QRectF() { }

GameObject::GameObject(float x, float y, float height, float width) 
: QRectF(x, y, height, width) { }

GameObject::GameObject(const QRectF &rect)
: QRectF(rect) { }

GameObject::~GameObject() { }

QRect GameObject::determineScaledRect(float scale) const {
    return determineScaledRect(scale, *this);
}

QRect GameObject::determineScaledRect(float scale, const QRectF &rect) const {
    QRect copy;
    copy.setX(floor(rect.x() * scale));
    copy.setY(floor(rect.y() * scale));
    copy.setWidth(round(rect.width() * scale));
    copy.setHeight(round(rect.height() * scale));
    return copy;
}

QRectF GameObject::determineScaledRectF(float scale) const {
    return determineScaledRectF(scale, *this);
}

QRectF GameObject::determineScaledRectF(float scale, const QRectF &rect) const {
    QRectF copy(rect);
    copy.setX(floor(rect.x() * scale));
    copy.setY(floor(rect.y() * scale));
    copy.setWidth(round(rect.width() * scale));
    copy.setHeight(round(rect.height() * scale));
    return copy;
}

QRect GameObject::getScaledRect(float scale) const {
    return getScaledRectF(scale).toRect();
}

QRectF GameObject::getScaledRectF(float scale) const {
    QRect rect;
    rect.setX(x() * scale);
    rect.setY(y() * scale);
    rect.setWidth(width() * scale);
    rect.setHeight(height() * scale);
    return rect;
}

}
