#pragma once

#include "Ghost.h"
#include "Board.h"
#include "Definitions.h"

namespace PacMan {
class Blinky : public Ghost {
public:
    Blinky(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Blinky();
    virtual void update(float delta) override;
};
}
