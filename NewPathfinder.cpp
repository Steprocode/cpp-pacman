#include "NewPathfinder.h"

namespace PacMan {
    
NewPathfinder::NewPathfinder(QString IDMask, Board &board)
: board(board), IDMask(IDMask) {
}

NewPathfinder::~NewPathfinder() {
    
}

std::vector<const Cell *> NewPathfinder::findRoute(unsigned int initRow, unsigned int initCol, unsigned int targetRow, unsigned int targetCol) const {
    const Cell *initCell = board.getCells()[initRow][initCol];
    const Cell *targetCell = board.getCells()[targetRow][targetCol];
    return findRoute(initCell, targetCell);
}

std::vector<const Cell *> NewPathfinder::findRoute(const Cell *initCell, const Cell *targetCell) const {
    Node root(initCell, targetCell, IDMask);
    Node *last = nullptr;
    
    while (last == nullptr) {
        last = root.dig();
    }
    return last->convertToArray();
}

std::vector<const Cell *> NewPathfinder::Node::convertToArray() const {
    std::vector<const Cell *> reverse;
    std::vector<const Cell *> result;
    
    const Node *node = const_cast<const Node*>(this);
    while (node != NULL) {
        reverse.push_back(node->cell);
        node = node->parent;
    }
    for (int counter = reverse.size() - 1; counter >= 0; counter--)
        result.push_back(reverse.at(counter));
    return result;
}


NewPathfinder::Node::Node(const Cell *cell, const Cell *targetCell, QString IDMask, const Node *parent) 
: cell(cell), targetCell(targetCell), IDMask(IDMask), parent(parent) {
    
}

NewPathfinder::Node::~Node() {
    
}

NewPathfinder::Node *NewPathfinder::Node::dig() {
    if (cell == targetCell)
        return this;
    if (isExplored) {
        Node *node = nullptr;
        for (Node &child: children){
            node = child.dig();
            if (node != nullptr)
                return node;
        }
    }
    else {
        vector<Cell *> neighborCells = cell->getNeighbors(IDMask);
        for (const Cell *neighbor: neighborCells) {
            if (isAncestor(neighbor) || isChild(neighbor))
                continue;
            
            children.push_back(Node(neighbor, targetCell, IDMask, this));
            if (neighbor == targetCell)
                return &children.back();
        }
        isExplored = true;
    }
    
    return nullptr;
}

bool NewPathfinder::Node::isAncestor(const Cell *cell) const {
    if (parent == nullptr)
        return false;
    if (parent->cell == cell)
        return true;
    return parent->isAncestor(cell);
}

bool NewPathfinder::Node::isChild(const Cell *cell) const {
    for (const Node &child: children) {
        if (child.cell == cell)
            return true;
    }
    return false;
}

}
