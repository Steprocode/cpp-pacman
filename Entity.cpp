#include "Entity.h"

namespace PacMan {

Entity::Entity(unsigned int row, unsigned int col, Board &board, QPointF navigatorPos, QChar initPixKey, SpawnAlignment spawnAlignment)
: GameObject(), board(board), spawnRow(row), spawnCol(col), navigatorPos(navigatorPos), initPixKey(initPixKey) {
    adjustSpawnAlignment(spawnAlignment);
    setWidth(board.getCellWidth());
    setHeight(board.getCellHeight());
    navigator.setSize(QSize(board.getCellWidth(), board.getCellHeight()));
    reset();
}

Entity::~Entity() {
    
}

void Entity::update(float delta) {
    float step = delta * speed;
    
    if (currentDirection != nextDirection) {
        Cell *cell = board.getCellContaining(getNaviOuterPoint(nextDirection));
        if (/*cell != NULL && */!isCellObstacle(*cell)) {
            if (currentDirection == HOLD) {
                cell = board.getCellContaining(center());
                navigator.moveLeft(cell->left());
                navigator.moveTop(cell->top());
                currentDirection = nextDirection;
            }
            else if (currentDirection == LEFT || currentDirection == RIGHT) {
                if (nextDirection == UP || nextDirection == DOWN) {
                    float deltaDistance = abs(cell->left() - navigator.left());
                    if (deltaDistance < step) {
                        navigator.moveLeft(cell->left());
                        refreshGeometry();
                        currentDirection = nextDirection;
                    }
                }
                else {
                    currentDirection = nextDirection;
                }
            }
            else if (currentDirection == UP || currentDirection == DOWN) {
                if (nextDirection == LEFT || nextDirection == RIGHT) {
                    float deltaDistance = abs(cell->top() - navigator.top());
                    if (deltaDistance < step) {
                        navigator.moveTop(cell->top());
                        refreshGeometry();
                        currentDirection = nextDirection;
                    }
                }
                else {
                    currentDirection = nextDirection;
                }
            }
        }
    }
    
    if (currentDirection != HOLD) {
        move(step, currentDirection);
        Cell *cell = board.getCellContaining(getNaviBorderPoint());
        if (/*cell != NULL && */isCellObstacle(*cell)) {
            if (currentDirection == LEFT) {
                navigator.moveLeft(cell->right());
            }
            else if (currentDirection == RIGHT) {
                navigator.moveRight(cell->left());
            }
            else if (currentDirection == UP) {
                navigator.moveTop(cell->bottom());
            }
            else if (currentDirection == DOWN) {
                navigator.moveBottom(cell->top());
            }
        }
        refreshGeometry();
        refreshPixmap(delta);
    }
    
}

void Entity::refreshGeometry() {
    ClippedPoint clippedPoint(navigator.topLeft(), board);
    navigator.moveTopLeft(clippedPoint.toPointF());
    moveTopLeft(navigator.topLeft());
    translate(-navigatorPos.x(), -navigatorPos.y());
}

void Entity::draw(float /*delta*/, float scale, QPainter &painter) const {
    QRect paintArea = determineScaledRect(scale);
    painter.drawPixmap(paintArea, pixmap);
    
    QRect leftFollower = paintArea;
    leftFollower.translate(-(board.width() * scale), 0);
    painter.drawPixmap(leftFollower, pixmap);
    
    QRect rightFollower = paintArea;
    rightFollower.translate((board.width() * scale), 0);
    painter.drawPixmap(rightFollower, pixmap);
    
    QRect topFollower = paintArea;
    topFollower.translate(0, -(board.height() * scale));
    painter.drawPixmap(rightFollower, pixmap);
    
    QRect bottomFollower = paintArea;
    bottomFollower.translate(0, (board.height() * scale));
    painter.drawPixmap(bottomFollower, pixmap);
}

void Entity::setPixmap(const QPixmap &pixmap) {
    this->pixmap = pixmap;
    setSize(pixmap.size());
}

void Entity::move(float deltaX, float deltaY) {
    navigator.translate(deltaX, deltaY);
    refreshGeometry();
}

void Entity::move(float step, Direction direction) {
    if (direction == LEFT)
        move(-step, 0);
    else if (direction == RIGHT)
        move(step, 0);
    else if (direction == UP)
        move(0, -step);
    else if (direction == DOWN)
        move(0, step);
}

void Entity::handleKeyEvent(QKeyEvent */*event*/) {

}

QPointF Entity::getNaviOuterPoint() const {
    return getNaviOuterPoint(currentDirection);
}

QPointF Entity::getNaviBorderPoint() const {
    return getNaviBorderPoint(currentDirection);
}

QPointF Entity::getNaviOuterPoint(Direction direction) const {
    if (direction == UP)
        return QPointF(navigator.left() + navigator.width() / 2.0, navigator.top() - navigator.height() / 2.0);
    else if (direction == DOWN)
        return QPointF(navigator.left() + navigator.width() / 2.0, navigator.bottom() + navigator.height() / 2.0);
    else if (direction == LEFT)
        return QPointF(navigator.left() - navigator.width() / 2.0, navigator.top() + navigator.height() / 2.0);
    else if (direction == RIGHT)
        return QPointF(navigator.right() + navigator.width() / 2.0, navigator.top() + navigator.height() / 2.0);
    return navigator.center();
}

QPointF Entity::getNaviBorderPoint(Direction direction) const {
    if (direction == UP)
        return QPointF(navigator.left() + navigator.width() / 2.0, navigator.top());
    else if (direction == DOWN)
        return QPointF(navigator.left() + navigator.width() / 2.0, navigator.bottom());
    else if (direction == LEFT)
        return QPointF(navigator.left(), navigator.top() + navigator.height() / 2.0);
    else if (direction == RIGHT)
        return QPointF(navigator.right(), navigator.top() + navigator.height() / 2.0);
    return navigator.center();
}

void Entity::reset() {
    currentDirection = HOLD;
    nextDirection = HOLD;
    navigator.moveTopLeft(QPointF(spawnCol * board.getCellWidth(), spawnRow * board.getCellHeight()));
    navigator.translate(spawnShift.x(), spawnShift.y());
    refreshGeometry();
}

void Entity::adjustSpawnAlignment(SpawnAlignment spawnAlignment) {
    if (spawnAlignment == LEFT_SIDED) {
        spawnShift.setX(LEFT_SPAWN_SHIFT_X);
        spawnShift.setY(LEFT_SPAWN_SHIFT_Y);
    }
    else if (spawnAlignment == RIGHT_SIDED) {
        spawnShift.setX(RIGHT_SPAWN_SHIFT_X);
        spawnShift.setY(RIGHT_SPAWN_SHIFT_Y);
    }
}

bool Entity::isCellObstacle(const Cell &cell) const {
    if (allowedCellIDs.contains(cell.getID()))
        return false;
    return true;
}

Entity::Direction Entity::getDirection() const {
    return currentDirection;
}

bool Entity::collides(const Entity &another) const {
    QRectF anotherRect(another.navigator);
    anotherRect.translate(-board.width(), -board.height());
    int rowsCheck = 3;
    int colsCheck = 3;
    for (int col = 0; col < colsCheck; col++) {
        anotherRect.moveTop(another.top() - board.height());
        for (int row = 0; row < rowsCheck; row++) {
            if (navigator.intersects(anotherRect))
                return true;
            anotherRect.translate(0, board.height());
        }
        
        anotherRect.translate(board.width(), 0);
    }
    
    return false;
}

}
