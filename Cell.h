#pragma once

#include <QLabel>
#include <QString>
#include <QPixmap>
#include <QChar>
#include <QMap>
#include <QPainter>
#include <QDebug>
#include <map>
#include <vector>
#include <iostream>
#include <AssetsLoader.h>
#include "GameObject.h"

namespace PacMan {

class Cell : public GameObject {
public:
    Cell(const Dictionary &dictionary);
    Cell(QChar ID, const Dictionary &dictionary);
    Cell(unsigned int row, unsigned int col, float width, float height, const Dictionary &dictionary);
    ~Cell() override;
    
    QChar getID() const;
    virtual void setID(QChar ID);
    virtual void update(float delta) override;
    virtual void draw(float delta, float scale, QPainter &painter) const override;
    
    vector<Cell *> getNeighbors(QString IDMask) const;
    unsigned int getRow() const;
    unsigned int getCol() const;
    Cell *leftCell = NULL, *rightCell = NULL, *topCell = NULL, *bottomCell = NULL;
private:
    QPixmap pixmap;
    QChar ID;
    const Dictionary &dictionary;
    unsigned int row, col;
};
ostream &operator<<(ostream &output, const Cell &cell);
}
