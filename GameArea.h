#pragma once

#include <QWidget>
#include <QString>
#include <QPainter>
#include <QTimer>
#include <QKeyEvent>
#include "Board.h"
#include "Definitions.h"
#include "GameManager.h"
#include "Player.h"


namespace PacMan {

class GameArea : public QWidget {
    Q_OBJECT
public:
    GameArea(QWidget *parent = 0);
    GameArea(const QString &tilesetDictionaryPath, const QString &tilesetPath, QWidget *parent = 0);
    ~GameArea();
    void loadMap(const QString &mapPath);
    void start();
    void refresh();
    const GameManager &getGameManager() const;
    virtual int heightForWidth(int width) const override;
    virtual bool hasHeightForWidth() const override;
signals:
    void emitOnGameEnd() const;
public slots:
    void tick();
protected:
    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
private:
    Board board;
    GameManager gameManager;
    QTimer timer;
};

}
