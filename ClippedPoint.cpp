#include "ClippedPoint.h"

namespace PacMan {

ClippedPoint::ClippedPoint(QRectF clip)
: QPointF(), clip(clip) {
    refresh();
}

ClippedPoint::ClippedPoint(QPointF qPoint, QRectF clip)
: QPointF(qPoint), clip(clip) {
    refresh();
}

ClippedPoint::ClippedPoint(float x, float y, QRectF clip)
: QPointF(x, y), clip(clip) {
    refresh();
}
    
ClippedPoint::~ClippedPoint() {
    
}

void ClippedPoint::refresh() {
    setX(x());
    setY(y());
}

QRectF ClippedPoint::getClip() const {
    return clip;
}

void ClippedPoint::setClip(QRectF clip) {
    this->clip = clip;
    refresh();
}

float ClippedPoint::x() const {
    return QPointF::x();
}

float ClippedPoint::y() const {
    return QPointF::y();
}

void ClippedPoint::setX(float x) {
//     if (x < clip.left()) {
//         float deltaX = abs(clip.right() - x);
//         x = x + clip.width() * (deltaX / clip.width());
//     }
//     if (x > clip.right()) {
//         float deltaX = abs(x - clip.left());
//         x = x - clip.width() * (deltaX / clip.width());
//     }
    
    while(x < clip.left())
        x += clip.width();
    while (x > clip.right())
        x -= clip.width();
    
    QPointF::setX(x);
}

void ClippedPoint::setY(float y) {
    while (y < clip.top())
        y += clip.height();
    while (y > clip.bottom())
        y -= clip.height();
//     if (y < clip.top()) {
//         float deltaY = abs(clip.bottom() - y);
//         y = y + clip.height() * (deltaY / clip.height());
//     }
//     if (y > clip.bottom()) {
//         float deltaY = abs(y - clip.top());
//         y = y - clip.height() * (deltaY / clip.height());
//     }
    
    QPointF::setY(y);
}

QPointF ClippedPoint::toPointF() const {
    return *this;
}

QPoint ClippedPoint::toPoint() const {
    return QPointF::toPoint();
}

}
