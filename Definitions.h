#pragma once

#define BASE_CELL_WIDTH 8
#define BASE_CELL_HEIGHT 8

#define DEFAULT_MAP_PATH "maps/default.map"
#define TILESET_DICTIONARY_PATH "assets/tileset.dict"
#define TILESET_PATH "assets/tileset.png"

#define FONT_PATH "assets/Seven Oh Ess 8x16.ttf"
#define FONT_NAME "Seven Oh Ess 8x16"

#define PINKY_TILES_PATH "assets/pinky.png"
#define CLYDE_TILES_PATH "assets/clyde.png"
#define BLINKY_TILES_PATH "assets/blinky.png"
#define INKY_TILES_PATH "assets/inky.png"
#define GHOST_DICT_PATH "assets/ghost.dict"
#define FRIGHTENED_GHOST_PATH "assets/frightened.png"

#define PACMAN_TILES_PATH "assets/pacman.png"
#define PACMAN_DICT_PATH "assets/pacman.dict"

#define RIGHT_SPAWN_SHIFT_X 4
#define RIGHT_SPAWN_SHIFT_Y 0
#define NAVIGATOR_POS_X 3
#define NAVIGATOR_POS_Y 2

#define LEFT_SPAWN_SHIFT_X -4
#define LEFT_SPAWN_SHIFT_Y 0

#define FPS 30
