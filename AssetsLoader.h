#pragma once

#include <map>
#include <QChar>
#include <QString>
#include <QPixmap>
#include <QFile>
#include <QDebug>
#include <QSize>
#include <QTextStream>
#include "Tiles.h"

namespace PacMan {
typedef map<QChar, QPixmap> Dictionary;
class AssetsLoader {
public:
    static Dictionary loadDictionary(const QString &tilesDictionaryPath, const QString &tilesPath);
    static QString readFile(const QString &path);
    static QChar **passInputToMatrix(unsigned int rows, unsigned int cols, const QString &text);
    static QSize determineSize(const QString &text);
};
    
}
