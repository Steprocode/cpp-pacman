#include "Clyde.h"

namespace PacMan {
Clyde::Clyde(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment) 
: Ghost(row, col, board, QPoint(NAVIGATOR_POS_X, NAVIGATOR_POS_Y), CLYDE_TILES_PATH, initPixKey, spawnAlignment) {
    speed = 40;
    mode = CHASE;
    target = PLAYER_BACK;
}

Clyde::~Clyde() {
    
}

void Clyde::update(float dt) {
    Ghost::update(dt);
}
}
