#pragma once

#include <algorithm>
#include <QChar>
#include <vector>
#include <QPainter>
#include <QKeyEvent>
#include <QObject>
#include "Board.h"
#include "Player.h"
#include "Cell.h"
#include "Ghost.h"
#include "Blinky.h"
#include "Pinky.h"
#include "Clyde.h"
#include "Inky.h"

namespace PacMan {
class GameManager : public QObject {
    Q_OBJECT
public:
    GameManager(QObject* parent = 0);
    GameManager(Board &board, QObject* parent = 0);
    ~GameManager();
    void spawnEntities();
    void update(float delta);
    void draw(float delta, float scale, QPainter &painter);
    void handleKeyEvent(QKeyEvent *event);
    
    int getBallsLeft() const;
    bool areAnyBallsLeft() const;
    void addScore(int increment = 1);
    void setScore(int score);
    int getScore() const;
    void reset();
    void updateBallsLeft();
signals:
    void scoreValueChanged(int score);
private:
    void handlePlayerCollision(const Ghost *collidingGhost);
    void checkCellForEntities(unsigned int row, unsigned int col, Board &board);
    void checkCellForBalls(unsigned int row, unsigned int col, Board &board);
    void setPlayerActions();
    void updateMode(float delta);
    float timeToModeChange = 20;
    Ghost::Mode globalMode = Ghost::SCATTER;
    vector<Ghost *> ghosts;
    Player *player = NULL;
    Board *board = NULL;
    bool endGame = false;
    int score = 0;
    int ballsLeft = -1;
};
}

