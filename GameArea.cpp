#include "GameArea.h"

namespace PacMan {
    
GameArea::GameArea(QWidget *parent)
: QWidget(parent), board(TILESET_DICTIONARY_PATH, TILESET_PATH), gameManager(board) {
    refresh();
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
    setFocusPolicy(Qt::StrongFocus);
    
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHeightForWidth(true);
    setSizePolicy(sizePolicy);
}

GameArea::GameArea(const QString &tilesetDictionaryPath, const QString &tilesetPath, QWidget *parent) 
: QWidget(parent), board(tilesetDictionaryPath, tilesetPath), gameManager(board) {
    refresh();
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
    setFocusPolicy(Qt::StrongFocus);
    
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHeightForWidth(true);
    setSizePolicy(sizePolicy);
}

GameArea::~GameArea() {
        
}

void GameArea::start() {
    gameManager.spawnEntities();
    gameManager.updateBallsLeft();
    timer.setInterval(1.0 / FPS * 1000);
    timer.start();
}

void GameArea::refresh() {
    setMinimumWidth(board.width());
    setMinimumHeight(board.height());
}

void GameArea::tick() {
    float delta = 1.0 / FPS;
    board.update(delta);
    gameManager.update(delta);
    update();
    if (!gameManager.areAnyBallsLeft()) {
        timer.stop();
        emit emitOnGameEnd();
    }
}

void GameArea::paintEvent(QPaintEvent */*event*/) {
    float delta = 1.0 / FPS;
    QPainter painter(this);
    float scaleX = 1.0 * width() / board.width();
    float scaleY = 1.0 * height() / board.height();
    float finalScale = scaleX;
    if (scaleY < finalScale)
        finalScale = scaleY;
    painter.setClipRect(QRectF(0, 0, board.width() * finalScale, board.height() * finalScale));
    
    painter.fillRect(board.getScaledRect(finalScale), Qt::black);
    board.draw(delta, finalScale, painter);
    gameManager.draw(delta, finalScale, painter);
}

void GameArea::loadMap(const QString &mapPath) {
    board.loadMap(mapPath);
    refresh();
}

void GameArea::keyPressEvent(QKeyEvent *event) {
    gameManager.handleKeyEvent(event);
}

const GameManager &GameArea::getGameManager() const {
    return gameManager;
}

int GameArea::heightForWidth(int width) const {
    return width * (1.0 * board.getRows() / board.getCols());
}

bool GameArea::hasHeightForWidth() const {
    return true;
}

}
