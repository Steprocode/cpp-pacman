#pragma once

#include "Ghost.h"
#include "Board.h"
#include "Definitions.h"

namespace PacMan {
class Clyde : public Ghost {
public:
    Clyde(unsigned int row, unsigned int col, Board &board, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Clyde();
    void update(float dt) override;
};
}
