// Created by Maciej Stępień

#include "Game.h"

namespace PacMan {
Game::Game(QWidget *parent)
: QWidget(parent) {
    gameArea = new GameArea();
    gameArea->loadMap(DEFAULT_MAP_PATH);
    
    QFontDatabase::addApplicationFont(FONT_PATH);
    QFont font = QFont(FONT_NAME, 18, QFont::Normal);
    
    topLayout = new QHBoxLayout();
    topLayout->setAlignment(Qt::AlignLeft);
    
    scoreLayout = new QVBoxLayout();
    scoreLayout->setAlignment(Qt::AlignLeft);
    
    scoreTitleLabel = new QLabel();
    scoreTitleLabel->setText("HIGH SCORE");
    scoreTitleLabel->setFont(font);
    scoreTitleLabel->setAlignment(Qt::AlignLeft);
    scoreTitleLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    
    scoreLabel = new QLabel();
    scoreLabel->setText("0");
    scoreLabel->setFont(font);
    scoreLabel->setAlignment(Qt::AlignCenter);
    scoreLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    
    finishLabel = new QLabel();
    finishLabel->setVisible(false);
    finishLabel->setText("Finished!");
    finishLabel->setFont(font);
    finishLabel->setAlignment(Qt::AlignLeft);
    finishLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    
    scoreLayout->addWidget(scoreTitleLabel);
    scoreLayout->addWidget(scoreLabel, Qt::AlignCenter);
    connect(&gameArea->getGameManager(), &GameManager::scoreValueChanged,
        this, &Game::refreshScoreLabel
    );
    connect(gameArea, &GameArea::emitOnGameEnd,
        this, &Game::displayFinishLabel
    );
    
    topLayout->addLayout(scoreLayout);
    topLayout->addSpacing(10);
    topLayout->addWidget(finishLabel);
    
    contents = new QVBoxLayout();
    contents->addLayout(topLayout, Qt::AlignLeft);
    contents->addWidget(gameArea, Qt::AlignHCenter);
    setLayout(contents);
    
    gameArea->setFocus();
}

void Game::refreshScoreLabel(int score) {
    scoreLabel->setText(QString::number(score));
}

void Game::displayFinishLabel() {
    finishLabel->setVisible(true);
}

int Game::heightForWidth(int width) const {
    return width * (1.0 * minimumHeight() / minimumWidth());
}

bool Game::hasHeightForWidth() const {
    return true;
}

QSize Game::sizeHint() const {
    return QSize(minimumWidth(), minimumHeight());
}

Game::~Game() {
    
}

void Game::start() {
    gameArea->start();
}

}

int main(int argc, char** argv) {
    srand(time(NULL));
    
    QApplication app(argc, argv);
    PacMan::Game game;
    game.show();
    game.start();
    
    return app.exec();
}
