#pragma once
#include <QImage>
#include <QImageReader>
#include <QString>

using namespace std;
namespace PacMan {
    
class Tiles {
public:
    Tiles(unsigned int rows, unsigned int cols, QString path);
    Tiles(QString path, unsigned int tileWidth, unsigned int tileHeight);
    ~Tiles();
    QImage load(unsigned int row, unsigned int col) const;
    unsigned int getTileWidth() const;
    unsigned int getTileHeight() const;
private:
    QString path;
    unsigned int rows = 0;
    unsigned int cols = 0;
    unsigned int tileWidth = 0;
    unsigned int tileHeight = 0;
};
}
