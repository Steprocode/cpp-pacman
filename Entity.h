#pragma once

#include <QPainter>
#include <QPointF>
#include <QPixmap>
#include <QSize>
#include <QKeyEvent>
#include <math.h>
#include "GameObject.h"
#include "Board.h"
#include "Definitions.h"


namespace PacMan {

class Entity : public GameObject {
public:
    enum Direction {UP, DOWN, LEFT, RIGHT, HOLD};
    enum SpawnAlignment {LEFT_SIDED, RIGHT_SIDED};
    Entity(unsigned int row, unsigned int col, Board &board, QPointF navigatorPos, QChar initPixKey, SpawnAlignment spawnAlignment);
    ~Entity() override;
    void adjustSpawnAlignment(SpawnAlignment spawnAlignment);
    virtual void update(float delta) override;
    virtual void draw(float delta, float scale, QPainter &painter) const override;
    virtual void handleKeyEvent(QKeyEvent *event);
    virtual void reset();
    Direction getDirection() const;
protected:
    virtual void setPixmap(const QPixmap &pixmap);
    virtual void refreshPixmap(float delta) = 0;
    virtual bool isCellObstacle(const Cell &cell) const;
    QPointF getNaviOuterPoint() const;
    QPointF getNaviBorderPoint() const;
    QPointF getNaviOuterPoint(Direction direction) const;
    QPointF getNaviBorderPoint(Direction direction) const;
    Board &board;
    Direction currentDirection = HOLD;
    Direction nextDirection = HOLD;
    int spawnRow, spawnCol;
    QPointF spawnShift;
    QPointF navigatorPos;
    QRectF navigator;
    QPixmap pixmap;
    QChar initPixKey;
    QString allowedCellIDs = "*@□PpBbCcIiVv";
    int speed = 60;
protected:
    bool collides(const Entity &another) const;
private:
    void refreshGeometry();
    void move(float deltaX, float deltaY);
    void move(float step, Direction direction);
};

}
